@extends('layouts.app') 

@section('content')
<div class="container">
	<div class="row">

		<div class="col-lg-12">
			
			<h1>Polls</h1>
			<hr>
			<div class="jumbotron">
			<button type="button button-new" class="btn btn-primary btn-sm float-right ">Add New</button>
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Poll Name</th>
							<th scope="col">Sharing Url</th>
							<th scope="col">Answers</th>
							<th scope="col">Top Candidate</th>
							<th scope="col">Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($polls as $poll)
						<tr class="table-secondary">
							<th scope="row">{{ $poll->name }}</th>
							<td><a href="/poll/{{$poll->hash}}" target="_blank">/poll/{{$poll->hash}}</a></td>
							<td> {{$poll->answer_count }}</td>
							<td> {{$poll->top_candidate }}</td>
							<td>
								<div class="btn-group" role="group" aria-label="Basic example">
									<button type="button button-new" class="btn btn-primary btn-sm ">edit</button>
									<button type="button button-new" class="btn btn-warning btn-sm ">delete</button>
								
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>

		</div>

	</div>
	<div class="modal" id="pollModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Modal body text goes here.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('scripts')	
	
@endsection