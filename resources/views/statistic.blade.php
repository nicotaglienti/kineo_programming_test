@extends('layouts.app') 

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1>Statistics for: {{$poll->name}}</h1>
			<hr>
            <div class="jumbotron">
                <div class="card border-secondary mb-3" >
                    <div class="card-header">Header</div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="state">Grouped by:</label>
                            <select class="custom-select" name="state" id="ChartBy"> 
                            <option selected="" value="0">All states:</option>
                            @foreach ($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row" id="verticalChartContainer">

                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row">
								<div class="col-md-12">
								    <a href="/"  class="btn btn-primary float-right">Back to Home</a>
								</div>		
							</div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>

@endsection
@section('scripts')	
	<script>
        const chartData = <?php echo json_encode($data); ?>;
        const pollData = <?php echo json_encode($poll->toArray()); ?>;
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="/js/statistics.js"></script>
@endsection