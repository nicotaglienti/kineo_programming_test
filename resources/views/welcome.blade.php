@extends('layouts.app') 

@section('content')
    <div class="container">
    	<div class="row justify-content-center">
    		<div class="col-md-12">
    			<h1 class="text-center">Welcome to Kineo programming test</h1>
    			<h2 class="text-center">you can login or register if you have not an account</h2>
    		</div>
    	</div>
    </div>
@endsection
