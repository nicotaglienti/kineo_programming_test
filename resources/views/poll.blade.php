@extends('layouts.app') 

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1>{{$poll->name}}</h1>
			<hr>
			<div class="jumbotron">
				<div class="card border-secondary mb-3">
				<div class="card-header">Please complete this short poll to see results</div>
					<div class="card-body">
						@if (Session::has('error'))
						<div class="alert alert-dismissible alert-warning">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						@foreach (Session::get('error') as $error)
						<p>{{$error}}</p>
						@endforeach
						</div>
						@endif
						<form action="/answer/{{$hash}}" method="post" >
						@csrf
							<div class="row">
								<div class="col-md-6">
									<h2 class="card-title">{{__('Please select you state:')}}</h2>
								</div>		
								<div class="col-md-6">
									<div class="form-group">
										<select class="custom-select" name="state"> 
										<option selected="" value="0">Please Select one state:</option>
										@foreach ($states as $state)
											@if (Session::has('state') && $state->id== Session::get('state'))
											<option value="{{$state->id}}" selected>{{$state->name}}</option>
											@else	
											<option value="{{$state->id}}">{{$state->name}}</option>
											@endif
										@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<h2 class="card-title">{{$poll->yes_no_question}}</h2>
								</div>		
								<div class="col-md-6">
									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" name="yes_no_question" id="customSwitch1" checked="">
										<label class="custom-control-label" for="customSwitch1"></label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<h2 class="card-title">{{$poll->candidates_question}}</h2>
								</div>		
								<div class="col-md-6">
									<div class="form-group">
										<select class="custom-select" name="candidates_question"> 
										<option  value="0">Please Select one candidate:</option>
										@foreach ($poll->Candidates as $candidate)
											<option value="{{$candidate->id}}"
											@if (Session::has('candidates_question') && $candidate->id== Session::get('candidates_question'))
											<option value="{{$candidate->id}}" selected>{{$candidate->name}}</option>
											@else	
											<option value="{{$candidate->id}}" >{{$candidate->name}}</option>
											@endif
										@endforeach
										</select>
									</div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-12">
								<a href="/" type="button" class="btn btn-warning ">Cancel</a>
								<button type="submit" class="btn btn-primary float-right">Submit</button>
								</div>		
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>

	</div>
	
@endsection
@section('scripts')	
	
@endsection