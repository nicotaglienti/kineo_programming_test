<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{

    /**
     * Get the Anwers.
     */
    public function Answers ()
    {
        return $this->hasMany('App\Answer', 'poll_id');
    }

    /**
     * Get the Candidates.
     */
    public function Candidates ()
    {
        return $this->belongsToMany('App\Candidate', 'poll_candidate', 'poll_id',
                'candidate_id');
    }

    /**
     */
    public function getAnswerCountAttribute ()
    {
        return count($this->Answers);
    }

    /**
     */
    public function getTopCandidateAttribute ()
    {
        $candidate_name = 'No Answers yet';
        if ($this->answer_count) {
            $answer = $this->Answers->countBy('candidates_question')->toArray();
            $candidate_id = array_keys($answer, max($answer))[0];
            $candidate_name = $this->Candidates->where('id', $candidate_id)
                ->pluck('name')
                ->first();
        }

        return $candidate_name;
    }
}
