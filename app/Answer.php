<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * Get the candidate.
     */
    public function Candidates ()
    {
        return $this->belongsTo('App\Candidate', 'candidates_question');
    }
     /**
     * Get the state.
     */
    public function States ()
    {
        return $this->belongsTo('App\State' , 'state_id');
    }
}
