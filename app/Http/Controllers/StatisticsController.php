<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Poll;
use App\State;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hash)
    {

        $poll = Poll::where('hash', $hash)->first();
        $states = State::all();

        $answers = Answer::where('poll_id', $poll->id)->with('States', 'Candidates')->get();  

        $data = $answers->map(function ($aux){
            return [
                    "poll_id"=> $aux->poll_id,
                    "state_id"=> $aux->state_id,
                    "state_name"=> $aux->States->name,
                    "yes_no_question"=> $aux->yes_no_question,
                    "candidates_question"=> $aux->candidates_question,
                    "candidate_name"=> is_object($aux->Candidates) ? $aux->Candidates->name : "",
            ];
        });
        return view('statistic',compact('data','poll','states'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
