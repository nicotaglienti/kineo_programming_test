<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
Use Redirect;
//models
use App\Answer;
use App\Poll;
use App\State;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hash)
    {

        $poll = Poll::where('hash',$hash)->with('Candidates')->first();
        
        if($poll==null){
            return redirect('/');
        }

        $states = State::all();

        return view('poll',compact('poll', 'hash', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $hash)
    {   
        $answer = $request->all();
        if(!$request->has('yes_no_question')){
            $answer['yes_no_question'] = 0;
        }else{
            $answer['yes_no_question'] = 1;
        }
        $errors =[];
        if ($answer['state']==0){
            $errors[]= "you must choose a state";
        }
        if ($answer['yes_no_question'] && $answer['candidates_question']== 0) {
            $errors[]= "you must choose a candidate";
        }
        if($errors!= []){
            Session::flash('error', $errors);
            Session::flash('candidates_question', $answer['candidates_question']);
            Session::flash('yes_no_question', $answer['yes_no_question']);
            Session::flash('state', $answer['state']);
            return redirect::back();
        }

        $new_answer = new Answer;
        $new_answer->poll_id = Poll::where('hash', $hash)->pluck('id')->first();
        $new_answer->state_id = $answer['state'];
        $new_answer->yes_no_question = $answer['yes_no_question'];
        $new_answer->candidates_question = $answer['candidates_question'];
        $new_answer->save();


        Session::flash('success', 'your answer was successfully saved ');
        return redirect('/statistic/'.$hash);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
