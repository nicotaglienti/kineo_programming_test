$(document).ready(function () {
	


    $('#ChartBy').change(async function () {
        updateVerticalChart();
    });
    $('#ChartBy').trigger('change');

});

const updateVerticalChart = async (filterChanged = null) => {

    let chartBy = $('#ChartBy').val();

    let FilteredChartData  = chartData;

    if(chartBy != 0){
        FilteredChartData  = chartData.filter(data => data.state_id == chartBy);
    }
    let labels = ['will not vote'];
    let datasets = [];
    let groupedData = [{
        candidate_id : 0,
        candidate_name : 'will not vote',
        votes_amount : 0,
    }];

    for (const iterator of FilteredChartData) {
        if (labels.indexOf(iterator.candidate_name) === -1) {
            labels.push(iterator.candidate_name);
        }
        if(iterator.yes_no_question ==0){
            let index = groupedData.findIndex(aux => aux.candidate_id == 0);
            groupedData[index].votes_amount += 1;
        }else{
            let index = groupedData.findIndex(aux => aux.candidate_id == iterator.candidates_question);
            if (index === -1) {
                groupedData.push({
                    candidate_id : iterator.candidates_question,
                    votes_amount : 1,
                })
            }else{
                groupedData[index].votes_amount += 1;
            }

        }

        
    }



    const answer_amount = chartData.length;
    let datasetCollumns = [];
    for (const iterator of groupedData) {
        datasetCollumns.push(roundTwoDecimal(iterator.votes_amount * 100 / answer_amount))
    }

    let colorA = randomizeInteger(50, 171);
    let colorB = randomizeInteger(105, 206);
    let colorC = randomizeInteger(180, 229);
    let backgroundColor = "rgba(" + colorA + ", " + colorB + ", " + colorC + ",0.4)";

    datasets.push({
        label: pollData.name,
        data:datasetCollumns,
        backgroundColor: backgroundColor,
    });

    $('#chartjs_barChart').detach();
    $('#verticalChartContainer').append('<canvas id="chartjs_barChart"  class="chartjs-render-monitor" ></canvas>');
    var ctx = document.getElementById("chartjs_barChart").getContext('2d');
    let myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: datasets
        }
    });

}


/**
 * 
 * 
 * 
 */
const roundTwoDecimal = (number) => {
    return Math.round((number + Number.EPSILON) * 100) / 100;
}


/*
	inclusive min (result can be equal to min value)
    exclusive max (result will not be to max value)
*/
const randomizeInteger = (min, max) => {
    if (max == null) {
        max = (min == null ? Number.MAX_SAFE_INTEGER : min);
        min = 0;
    }

    min = Math.ceil(min);  // inclusive min
    max = Math.floor(max); // exclusive max

    if (min > max - 1) {
        throw new Error("Incorrect arguments.");
    }

    return min + Math.floor((max - min) * Math.random());
}