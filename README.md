# Installation

1) from main folder copy and rename file ```cp .env.example .env```

2) Create mysql db on your system and set up .env vars: 

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=kineo
DB_USERNAME=root
DB_PASSWORD=
```

3) run command  ```#composer install```

4) run command  ```#php artisan config:cache```

5) run command  ```#php artisan migrate:refresh --seed```

6) to lunch the web use command  ```#php artisan serve```

7) For any troubleshooting, visit the official Laravel website ```https://laravel.com/docs/5.8/installation```

## login access:

User: admin@admin.com
Password: admin



## brief description:

As a registered user, you can access the list of polls from the top menu, you will see a URL to share and start collecting poll answers, after answering the poll the results will be shown and you can filter them by country or state



## functional requirements not included:
```
*crud of polls
*crud of candidates
*crud of states
```