<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/statistics', 'StatisticsController@index')->name('statistics');

Route::middleware(['auth'])->group(function () {
    Route::get('/candidates', 'CandidatesController@index')->name('candidates');
    Route::get('/polls', 'PollsController@index')->name('polls');
});

Route::get('/poll/{hash}', 'AnswersController@create')->name('pollsQuestions');
Route::post('/answer/{hash}', 'AnswersController@store')->name('newAnswer');
Route::get('/statistic/{hash}', 'StatisticsController@show')->name('showStatistic');