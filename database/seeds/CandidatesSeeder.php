<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CandidatesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $candidates = [
                1 => 'Ivana Doover',
                2 => 'Jeremy Jameson',
                3 => 'Steve Lowenthal'
        ];

        foreach ($candidates as $key => $candidate) {
            DB::table('candidates')->insert(
                    [
                            'id' => $key,
                            'name' => $candidate,
                            'user_id'=> 1
                    ]);
            DB::table('poll_candidate')->insert(
                    [
                            'poll_id' => 1,
                            'candidate_id' => $key
                    ]);
        }
    }
}
