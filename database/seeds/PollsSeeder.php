<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PollsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table('polls')->insert(
                [
                        'id' => 1,
                        'name' => 'US Presidential Election',
                        'hash' => Str::random(10),
                        'yes_no_question' => 'Are you going to vote?',
                        'candidates_question' => 'Who are you going to vote for?'
                ]);
    }
} 
