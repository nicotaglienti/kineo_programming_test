<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('polls',
                function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('hash');
                    $table->string('name');
                    $table->string('yes_no_question');
                    $table->string('candidates_question');
                    $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('polls');
    }
}
