<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePollResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('answers',
                function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->integer('poll_id');
                    $table->integer('state_id');
                    $table->boolean('yes_no_question');
                    $table->integer('candidates_question');
                    $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('answers');
    }
}
