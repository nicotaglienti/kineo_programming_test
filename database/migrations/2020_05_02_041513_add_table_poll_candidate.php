<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePollCandidate extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('poll_candidate',
                function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->integer('poll_id');
                    $table->integer('candidate_id');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('poll_candidate');
    }
}
